#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

using namespace std;

int main(void) {
  cv::VideoCapture cap("videoTest.mp4");

  if(cap.isOpened() == false) {
    cout << "Failed to open video" << endl;
    return -1;
  }

  double fps = cap.get(cv::CAP_PROP_FPS);
  cout << "Frames per second: " << fps << endl;

   string windowName = "OPENCV VIDEO";
   string windowName2 = "OPENCV VIDEO 2";
   cv::namedWindow(windowName, cv::WINDOW_NORMAL);
   cv::namedWindow(windowName2, cv::WINDOW_NORMAL);
  
  cv::Mat frame;
  while(cap.read(frame)) {
    // cv::Mat frame = cv::imread("snelweg.jpg");

    cv::Mat frameBlurred, frameCanny, frameWarped;
    cv::GaussianBlur(frame, frameBlurred, cv::Size(5, 5), 0);
    cv::Canny(frameBlurred, frameCanny, 50, 150);

    cv::Size frameSize = frame.size();
    // cout << "Frame Resolution: " << frameSize.width << "x" << frameSize.height << endl;

    cv::Point p1(frameSize.width * 0.15, frameSize.height - 1);
    cv::Point p2(frameSize.width * 0.85, frameSize.height - 1);
    cv::Point p3(frameSize.width * 0.45, frameSize.height * 0.70);
    cv::Point p4(frameSize.width * 0.55, frameSize.height * 0.70);

    // cout << "Center X: " << frameSize.width / 2 << endl;
    // cout << "1% X: " << frameSize.width  * 0.01 << endl;
    // cout << "45% X: " << frameSize.width * 0.45 << endl;
    // cout << "55% X: " << frameSize.width * 0.55 << endl;


    cv::circle(frame, p1, 15, cv::Scalar(255, 0, 0), cv::FILLED);
    cv::circle(frame, p2, 15, cv::Scalar(255, 0, 0), cv::FILLED);

    cv::circle(frame, p3, 15, cv::Scalar(255, 0, 0), cv::FILLED);
    cv::circle(frame, p4, 15, cv::Scalar(255, 0, 0), cv::FILLED);

    cv::Point2f srcPoints[4] = {p1, p2, p3, p4};
    cv::Point2f destPoints[4] = {cv::Point2f(300, frameSize.height -1), cv::Point2f(frameSize.width - 300, frameSize.height - 1), cv::Point2f(300, 0), cv::Point2f(frameSize.width - 300, 0)};

    cv::Mat M = cv::getPerspectiveTransform(srcPoints, destPoints);

    cv::warpPerspective(frameCanny, frameWarped, M, cv::Size(frameSize.width, frameSize.height), cv::INTER_LINEAR);

    // Histogram

    int midPoint = frameSize.width / 2;
    int quaterPoint = midPoint / 2;
    int leftxBase = 0; // Max value histogram left of midPoint;
    int rightxBase = 1000; // Max value histogram right of midPoint;
    
    int maxValue = 0;
    for(int i = quaterPoint; i < midPoint; i++) {
      int sum = 0;
      for(int j = frameSize.height / 2; j < frameSize.height; j++) {
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 0];
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 1];
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 2];
      }
      if(sum > maxValue) {
        maxValue = sum;
        leftxBase = i;
      }
    }
    maxValue = 0;
    for(int i = midPoint; i < frameSize.width - quaterPoint; i++) {
      int sum = 0;
      for(int j = frameSize.height / 4; j < frameSize.height; j++) {
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 0];
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 1];
        sum += frameWarped.data[frameWarped.channels() * frameSize.width * j + i + 2];
      }
      if(sum > maxValue) {
        maxValue = sum;
        rightxBase = i;
      }
    }

    cout << "Left  Base X: " << leftxBase << endl;
    cout << "Right Base X: " << rightxBase << endl;

    cv::circle(frameWarped, cv::Point(leftxBase, frameSize.height - 1), 15, cv::Scalar(255, 255, 255), cv::FILLED);
    cv::circle(frameWarped, cv::Point(rightxBase, frameSize.height - 1), 15, cv::Scalar(255, 255, 255), cv::FILLED);

    // int leftxCurrent = leftxBase, rightxCurrent = rightxBase;

    // #define NUMBER_OF_WINDOWS 10
    // int windowHeight = frameSize.height / NUMBER_OF_WINDOWS;
    // int windowWidth = 80; // width left and width right of center
    // int minPixToRecenter = 40;
    

    //Three lists: leftLane indexes, rightlane indexes, window data

    cv::imshow(windowName, frame);
    cv::imshow(windowName2, frameWarped);

    if(cv::waitKey((int)fps) == 'q') {
      cout << "Video stopped by user" << endl;
      break;
    }
  }

  // cv::waitKey(0);
  // cv::destroyWindow(windowName);

  return 0;
}